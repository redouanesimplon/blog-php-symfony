<?php

namespace App\Controller;

use App\Entity\Post;
use App\Form\PostType;
use App\Repository\PostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PostAddController extends AbstractController
{

    /**
     * @Route("/form-post", name="form_post")
     */
    public function formPost(Request $request)
    {
        
        $post = null;
        
        $title = $request->get("title");
        $author= $request->get("author");
        $content = $request->get("content");
        
        if ($author && $title && $content) {
           
            $post = new Post($title, $author, $content);
            
            
            $repo = new PostRepository();
            $repo->add($post);
        }
     
        return $this->render("form-post.html.twig", [            
            'post' => $post
        ]);
    }
     /**
     * On peut faire des routes avec des paramètres à l'intérieur,
     * ces paramètres devront être entourés d'accolades
     * @Route("/post/{id}", name="one_post")
     */
    public function onePost(int $id) {
        $repo = new PostRepository();
        $post = $repo->findById($id);
        dump($post);
        return $this->render('one-post.html.twig', [
            'post' => $post
        ]);
    }
}