<?php

namespace App\Controller;

use App\Repository\PostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ShowPostController extends AbstractController {

    /**
     * une route vide ou avec juste "/" sera accessible directement
     * sur la racine du projet (http://localhost:8000)
     * @Route("", name="home")
     */
    public function index(PostRepository $repo) { 
      
        $posts = $repo->findAll();
        
        return $this->render('show-post.html.twig', [
            'posts' => $posts
        ]);
    }

    /**
     * On peut faire des routes avec des paramètres à l'intérieur,
     * ces paramètres devront être entourés d'accolades
     * @Route("/post/{id}", name="one_post")
     */
    public function onePost(int $id) { 
        $repo = new PostRepository();
        $post = $repo->findById($id);
       
        return $this->render('one-post.html.twig', [
            'post' => $post
        ]);
    }

   
/**
     * @Route("/del/{id}", name="deletepost")
     */
    public function delete(int $id)
    {
        $repo = new PostRepository();
        $repo->deleteArticle($id);
        return $this->redirectToRoute('home');
    }

}