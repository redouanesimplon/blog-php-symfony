<?php

namespace App\Repository;

use App\Entity\Post;

/**
 * Un Repository, ou DAO (terme plus "pro", Data Access Object) est une
 * class dont le but est de concentré les appels à la base de données,
 * ainsi, le reste de l'application dépendra des DAO et on aura pas 
 * des appels bdd disseminés partout dans l'appli (comme ça, si jamais 
 * la table change, on le sgbd ou autre, on aura juste le DAO à 
 * modifier et pas tous les endroits où on aurait fait ces appels)
 */

class PostRepository
{
    private $pdo;

    public function __construct() {
    
        $this->pdo = new \PDO(
            'mysql:host='.$_ENV['DATABASE_HOST'].';dbname=' . $_ENV['DATABASE_NAME'],
            $_ENV['DATABASE_USERNAME'],
            $_ENV['DATABASE_PASSWORD']
        );
    }

    /**
     * Méthode qui va aller chercher toutes les posts
     * présentent dans la base de données et les convertir
     * en instance de la classe Post
     * @return Post[] les posts contenues dans la bdd
     */
    public function findAll(): array
    {
         /** 
         * L'instance de PDO représente une connexion à la base de
         * données. Elle attend en argument le serveur sur laquelle
         * se trouve notre bdd (localhost), le nom de la bdd à laquelle
         * on veut se connecter (introduction), puis le username et le
         * password pour se connecter à cette base de données. (il 
         * serait préférable de mettre ces informations dans un fichier
         * de configuration hors du php exemple:".env.local")
         */
// Ici je cree une instance qui va récuperer tout les post present dans ma base de données 
        $query = $this->pdo->prepare('SELECT * FROM post');
    
        $query->execute();
        $results = $query->fetchAll();
        $list = [];
        foreach ($results as $line) {
            $post = $this->sqlToPost($line);
            $list[] = $post;
        }
        return $list;
    }
   
    public function add(Post $post): void {
         /**
         * la je crée la requête pour faire une insertion, et dans cette
         * requête pour les valeurs à insérer dans la bdd, on va plutôt
         * mettre des placeholders (:title, :author ...) qu'on
         * pourra assigner par la suite
         */

        $query = $this->pdo->prepare('INSERT INTO post (title,author,content) VALUES (:title,:author,:content)');
        
        $query->bindValue(':title', $post->getTitle(), \PDO::PARAM_STR);
        $query->bindValue(':author', $post->getAuthor(), \PDO::PARAM_STR);
        $query->bindValue(':content', $post->getContent(), \PDO::PARAM_STR);
        
        $query->execute();
        
        $post->setId(intval($this->pdo->lastInsertId()));
    }

  
    public function findById(int $id): ?Post {
        /**
     * Méthode permettant de récupérer une personne spécifique en utilisant
     * son id. Si le post n'existe pas, on renvoie null
     */

        
        $query = $this->pdo->prepare('SELECT * FROM post WHERE id=:idPlaceholder');

        $query->bindValue(':idPlaceholder', $id, \PDO::PARAM_INT);
        
        $query->execute();
       
        $line = $query->fetch();
        
        if($line) {
            
            return $this->sqlToPost($line);
            
        }
    
        return null;

    }
    public function deleteArticle(int $id)
    {
        // Ici une méthode qui me permet de supprimer un article de ma bdd 
        
        $delete = $this->pdo->prepare('DELETE FROM post WHERE id=:id');
        $delete->bindValue('id', $id, \PDO::PARAM_INT);
        $delete->execute();
    }
   
    private function sqlToPost(array $line):Post {
       
        return new Post($line['title'], $line['author'], $line['content'], $line['id']);
    }
}
