<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* form-post.html.twig */
class __TwigTemplate_9c2cf1ac94c85076e9eff6adf26b8e1aa44bc5a57eaae85e9590b0286a466cff extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "form-post.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">

<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
    <title>CAR DREAM</title>
    <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css\" integrity=\"sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T\" crossorigin=\"anonymous\">
</head>

<body>
    <nav class=\"navbar navbar-expand-lg navbar-dark bg-dark \">
        <div class=\"container-fluid\">
            <a class=\"navbar-brand d-flex justify-content-start text-danger\" href=\"";
        // line 15
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("home");
        echo "\">Car Dream</a>";
        if ((isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 15, $this->source); })())) {
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 15, $this->source); })()), "title", [], "any", false, false, false, 15), "html", null, true);
            echo " ";
        }
        // line 16
        echo "            <a class=\"navbar-brand d-flex justify-content-end text-danger\" href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("form_post");
        echo "\">Add post</a>
        </div>
    </nav>
    ";
        // line 19
        if ((isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 19, $this->source); })())) {
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 19, $this->source); })()), "title", [], "any", false, false, false, 19), "html", null, true);
            echo " ";
        }
        // line 20
        echo "
    <div class=\"container-fluid\">

        <form>


            <label for=\"title \" class=\"font-weight-bold text-uppercase\">Title</label>
            <input type=\"text \" class=\"form-control col-sm-3 \" id=\"title\" name=\"title\">


            <label for=\"author \" class=\"font-weight-bold text-uppercase\">author</label>
            <input type=\"text \" class=\"form-control col-sm-3\" id=\"author\" name=\"author\">




            <label for=\"content \" class=\"font-weight-bold text-uppercase\">Content</label>
            <textarea class=\"form-control \" name=\"content\" id=\"content\" rows=\"3 \"></textarea>
            <button type=\"submit\" class=\"text-uppercase btn btn-danger \">ADD</button>


        </form>
    </div>





</body>

</html>";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "form-post.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  77 => 20,  71 => 19,  64 => 16,  56 => 15,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html lang=\"en\">

<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
    <title>CAR DREAM</title>
    <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css\" integrity=\"sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T\" crossorigin=\"anonymous\">
</head>

<body>
    <nav class=\"navbar navbar-expand-lg navbar-dark bg-dark \">
        <div class=\"container-fluid\">
            <a class=\"navbar-brand d-flex justify-content-start text-danger\" href=\"{{path('home')}}\">Car Dream</a>{% if post %} {{post.title}} {% endif %}
            <a class=\"navbar-brand d-flex justify-content-end text-danger\" href=\"{{path('form_post')}}\">Add post</a>
        </div>
    </nav>
    {% if post %} {{post.title}} {% endif %}

    <div class=\"container-fluid\">

        <form>


            <label for=\"title \" class=\"font-weight-bold text-uppercase\">Title</label>
            <input type=\"text \" class=\"form-control col-sm-3 \" id=\"title\" name=\"title\">


            <label for=\"author \" class=\"font-weight-bold text-uppercase\">author</label>
            <input type=\"text \" class=\"form-control col-sm-3\" id=\"author\" name=\"author\">




            <label for=\"content \" class=\"font-weight-bold text-uppercase\">Content</label>
            <textarea class=\"form-control \" name=\"content\" id=\"content\" rows=\"3 \"></textarea>
            <button type=\"submit\" class=\"text-uppercase btn btn-danger \">ADD</button>


        </form>
    </div>





</body>

</html>", "form-post.html.twig", "/var/www/html/blog-php-symfony/blog./templates/form-post.html.twig");
    }
}
