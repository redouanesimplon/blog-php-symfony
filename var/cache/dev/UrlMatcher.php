<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/form-post' => [[['_route' => 'form_post', '_controller' => 'App\\Controller\\PostAddController::formPost'], null, null, null, false, false, null]],
        '/' => [[['_route' => 'home', '_controller' => 'App\\Controller\\ShowPostController::index'], null, null, null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/_error/(\\d+)(?:\\.([^/]++))?(*:35)'
                .'|/post/([^/]++)(*:56)'
                .'|/del/([^/]++)(*:76)'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        35 => [[['_route' => '_twig_error_test', '_controller' => 'twig.controller.preview_error::previewErrorPageAction', '_format' => 'html'], ['code', '_format'], null, null, false, true, null]],
        56 => [[['_route' => 'one_post', '_controller' => 'App\\Controller\\ShowPostController::onePost'], ['id'], null, null, false, true, null]],
        76 => [
            [['_route' => 'deletepost', '_controller' => 'App\\Controller\\ShowPostController::delete'], ['id'], null, null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
